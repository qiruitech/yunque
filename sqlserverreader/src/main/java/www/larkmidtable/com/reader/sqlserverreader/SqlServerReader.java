package www.larkmidtable.com.reader.sqlserverreader;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import www.larkmidtable.com.model.DbTaskResult;
import www.larkmidtable.com.reader.AbstractDBReader;

/**
 * @author fei
 * @description: sqlserver 读工具
 * @date 2022-11-15
 */
public class SqlServerReader extends AbstractDBReader {

	private static final Logger logger = LoggerFactory.getLogger(SqlServerReader.class);

	@Override
	protected AbstractDbReadTask newDbReadTaskInstance() {
		return new SqlServerReaderTask();
	}

	/**
	 * 实现SQLSEVER的分页
	 */
	@Override
	public List<String> defaultInputSplits(String column, int count, int bcount) {
		String originInput = String.format("select %s from %s", configBean.getColumn(), configBean.getTable());
		List<String> splits = new ArrayList<>();
		if (count > 0) {
			// 线程数
			int threadSize = this.getConfigBean().getThread();
			// 页大小
			Integer limitSize = count / threadSize;
			Integer lastLimit = Integer.MAX_VALUE;
			for (int i = 0; i < threadSize; i++) {
				boolean lastPage = i == threadSize - 1;
				if (lastPage) {
					lastLimit = limitSize + count % threadSize;
				}
				// 页大小
				int pageSize = lastPage ? lastLimit : limitSize;
				// 偏移量
				int offset = i * limitSize;

				StringBuilder builder = new StringBuilder("SELECT " + column + " FROM (");
				builder.append(originInput).append(") t ").append(" order by ").append(configBean.getSplitPk())
						.append(" offset ").append(offset).append(" rows fetch next ").append(pageSize)
						.append(" rows only");
				splits.add(builder.toString());
			}
		} else {
			splits.add(originInput);
			logger.info("originInput： " + originInput);
		}

		return splits;
	}

	public class SqlServerReaderTask extends AbstractDbReadTask {
		// 使用默认实现
	}

}
