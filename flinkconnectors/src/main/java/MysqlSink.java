import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 *
 *
 * @Date: 2023/11/15 23:21
 * @Description:
 **/
public class MysqlSink extends RichSinkFunction<VehicleAlarm> {
	Connection conn = null;
	PreparedStatement ps = null;
	String url = "jdbc:mysql://localhost:3306/alarm-sc?useUnicode=true&characterEncoding=utf-8&useSSL=false";
	String username = "root";
	String password = "root";

	/**
	 * 首次sink时执行，且创建到销毁只会执行一次
	 * @param parameters
	 * @throws Exception
	 */
	@Override
	public void open(Configuration parameters) throws Exception {
		// 获取mysql 连接
		conn = DriverManager.getConnection(url, username, password);
		// 关闭自定提交
		conn.setAutoCommit(false);
	}

	/**
	 * 数据源无数据，sink关闭时执行，且创建到销毁只会执行一次
	 * @throws Exception
	 */
	@Override
	public void close() throws Exception {
		if (conn != null) {
			conn.close();
		}
		if (ps != null) {
			ps.close();
		}
	}

	/**
	 * 数据输出时执行，每一个数据输出时，都会执行此方法
	 * @param value
	 * @param context
	 * @throws Exception
	 */
	@Override
	public void invoke(VehicleAlarm value, Context context) throws Exception {
		String sql = "insert into vehicle_alarm_202104 (`id`,`license_plate`,`plate_color`,`device_time`,`zone`) " +
				"values(?,?,?,?,?)";
		ps = conn.prepareStatement(sql);
		ps.setString(1, value.getId());
		ps.setString(2, value.getLicensePlate());
		ps.setString(3, value.getPlateColor());
		ps.setLong(4, value.getDeviceTime());
		ps.setString(5, value.getZone());
		// 执行语句
		ps.execute();
		// 提交
		conn.commit();
	}
}
