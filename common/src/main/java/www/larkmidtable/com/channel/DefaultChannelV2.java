package www.larkmidtable.com.channel;

import www.larkmidtable.com.element.Record;
import www.larkmidtable.com.queue.DefaultQueueV2;
import www.larkmidtable.com.transformer.TransformerExecution;

import java.util.List;
import java.util.Queue;

/**
 * @Description 默认通道
 * @Date 2023/8/1
 */
public class DefaultChannelV2 extends ChannelV2 {

    public DefaultChannelV2(List<TransformerExecution> transformerExecutionList) {
        Queue<Record> queue = new DefaultQueueV2(transformerExecutionList);
        this.setQueue(queue);
    }
}
