package www.larkmidtable.com.reader;

import com.alibaba.fastjson.JSONObject;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import www.larkmidtable.com.bean.ConfigBean;
import www.larkmidtable.com.channel.Channel;
import www.larkmidtable.com.element.Column;
import www.larkmidtable.com.element.DefaultRecord;
import www.larkmidtable.com.element.Record;
import www.larkmidtable.com.element.Transformer;
import www.larkmidtable.com.exception.YunQueException;
import www.larkmidtable.com.log.LogRecord;
import www.larkmidtable.com.model.DbTaskParams;
import www.larkmidtable.com.model.DbTaskResult;
import www.larkmidtable.com.model.TaskResult;
import www.larkmidtable.com.util.JVMUtil;

import java.sql.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbstractDBReader extends Reader {

    private static Logger logger = LoggerFactory.getLogger(AbstractDBReader.class);
    /**
     * 拆分批量默认值，可考虑覆盖配置
     */
    protected final Integer DEFAULT_BATCH_SIZE = 10000;

    ExecutorService executorService;
    protected List<AbstractDbReadTask> tasks;
    protected List<CompletableFuture<TaskResult>> taskFutureResults;

    protected static CountDownLatch countDownLatch;

    protected Connection newConnection() {
        try {
            return DriverManager.getConnection(configBean.getUrl(), configBean.getUsername(), configBean.getPassword());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void init(int pos, int count) {
        logRecord = LogRecord.newInstance();
        logRecord.start(String.format("%s 初始化", this.getClass().getSimpleName()));
        Integer threadNum = getConfigBean().getThread();
        executorService = Executors.newFixedThreadPool(threadNum, new ThreadFactory() {
            private final AtomicInteger threadNumber = new AtomicInteger(1);

            @Override
            public Thread newThread(@NonNull Runnable r) {
                return new Thread(r, "reader-pool-" + threadNumber.getAndIncrement());
            }
        });
        tasks = new ArrayList<>(threadNum);
        int bcount =count * pos;
        tasks.addAll(getDbReadTasks(count,bcount));
        countDownLatch = new CountDownLatch(threadNum);
        logRecord.end();
    }

    protected List<AbstractDbReadTask> getDbReadTasks(int count,int bcount) {
        ArrayList<AbstractDbReadTask> mySqlReadTasks = new ArrayList<>();
        for (String inputSplit : createInputSplits(count,bcount)) {
            mySqlReadTasks.add(newDbReadTaskInstance().setDbReadTask(new DbTaskParams(getConfigBean(), inputSplit)));
        }
        return mySqlReadTasks;
    }

    protected abstract AbstractDbReadTask newDbReadTaskInstance();

    @Override
    public void read(Queue<Record> queue) {
        logRecord.start(String.format("%s 读取数据", this.getClass().getSimpleName()));
        this.queue = queue;
        taskFutureResults = new ArrayList<>();
        for (AbstractDbReadTask task : tasks) {
            CompletableFuture<TaskResult> future = CompletableFuture.supplyAsync(task, executorService);
            taskFutureResults.add(future);
        }
    }

    /**
     * 获取总条数
     * 目的是做SQL拆分
     *
     * @return 总数据条数
     */
    public int count() {
        try (Connection connection = newConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT count(1) FROM " + configBean.getTable());
             ResultSet resultSet = preparedStatement.executeQuery()) {
            resultSet.next();
            return resultSet.getInt(1);
        } catch (Exception e) {
            throw new YunQueException("获取数据源表数据条数出错", e);
        }
    }

    @Override
    public String[] createInputSplits(int count,int bcount) {
        LogRecord logRecord = LogRecord.newInstance();
        logRecord.start(String.format("%s 任务分片", configBean.getPlugin()));
        List<String> results = defaultInputSplits(configBean.getColumn(), count, bcount);
        logRecord.end();
        logger.debug("分片逻辑：\n{}", String.join("\n", results));
        String[] array = new String[results.size()];
        return results.toArray(array);
    }

    public List<String> defaultInputSplits(String column,int count,int bcount) {
        String originInput = String.format("select %s from %s", configBean.getColumn(), configBean.getTable());
        List<String> splits = new ArrayList<>();
        if (count > 0) {
            int threadSize = this.getConfigBean().getThread();
            int perSize = count / configBean.getThread();
            int lastSize = perSize + count % configBean.getThread();
            for (int i = 0; i < threadSize; i++) {
                int limitStart = i * perSize + bcount;
                int limitSize = i == threadSize - 1 ? lastSize : perSize;
                String builder = "SELECT " + column + " FROM ( " + " " + originInput + " ) t" + " " + "LIMIT" +
                        " " + limitStart + "," + limitSize;
                splits.add(builder);
                logger.info("builder： "+builder);
            }
        } else {
            splits.add(originInput);
			logger.info("originInput： "+originInput);
        }

        return splits;
    }


    /**
     * 开启批量多线程读
     *
     * @param connection
     * @param inputSplits
     * @return
     */
    @Deprecated
    public void batchStartRead(Connection connection, String[] inputSplits) throws Exception {
        CountDownLatch countDownLatch = new CountDownLatch(inputSplits.length);
        for (int i = 0; i < inputSplits.length; i++) {
            String inputSplit = inputSplits[i];
            // 后续可改成线程池
            new Thread(() -> {
                try {
                    defaultSingleStartRead(connection, inputSplit);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                } finally {
                    countDownLatch.countDown();
                }
            }).start();
        }
        // 等待所有的子线程运行完毕
        countDownLatch.await();
        // 发送
        // TODO 是否发送读取结束标识符，告诉下游？
    }

    /**
     * 默认的线程读写处理逻辑，数据库可选择覆写逻辑，实现自己的读写
     *
     * @param connection
     * @param inputSplit
     * @throws Exception
     */
    @Deprecated
    public void defaultSingleStartRead(Connection connection, String inputSplit) throws Exception {
        List<String> records = new ArrayList<>();
        // String sql = String.format("select * from %s", configBean.getTable());
        PreparedStatement preparedStatement = connection.prepareStatement(inputSplit);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Map<String, Object> map = new HashMap<>();
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                map.put(metaData.getColumnName(i), resultSet.getObject(metaData.getColumnName(i)));
            }
            records.add(JSONObject.toJSONString(map));
        }
        logger.info("添加到队列的记录条数{}", records.size());
        // 责任链模式执行数据清洗/转换
        if (records.size() != 0) {
            Channel.getQueue().add(records);
        }
    }

    @Deprecated
    public List<String> defaultInputSplits(String column, String originInput) {
        List<String> splits = new ArrayList<>();
        int count = count();
        if (count > 0 && 1 == 1) {// 1==1 后续可开启切分SQL配置参数
            // 拆分的大小
            int size = this.getConfigBean().getThread();
            int lastCount = count % DEFAULT_BATCH_SIZE;
            for (int i = 0; i < size; i++) {
                StringBuilder builder = new StringBuilder("SELECT " + column + " FROM ( ");
                builder.append(" ").append(originInput).append(" ) t").append(" ").append("LIMIT");
                int limitStart = i * DEFAULT_BATCH_SIZE;
                int j = i + 1;
                if (j == size) {
                    builder.append(" ").append(limitStart).append(",").append(lastCount);
                } else {
                    builder.append(" ").append(limitStart).append(",").append(DEFAULT_BATCH_SIZE);
                }
                splits.add(builder.toString());
            }
        } else {
            splits.add(originInput);
        }
        return splits;
    }

    @Override
    public void stop() {
        JVMUtil.shutdownThreadPool(executorService);
        logRecord.totalStatistics(taskFutureResults);
    }

    public abstract class AbstractDbReadTask extends ReadTask<DbTaskParams, DbTaskResult> {

        protected Connection connection;

        protected String splitSql;

        protected AbstractDbReadTask setDbReadTask(DbTaskParams taskParams) {
            this.taskParams = taskParams;
            this.splitSql = taskParams.getSplitSql();
            return this;
        }

        @Override
        public void preProcess() {
            ConfigBean configBean = this.taskParams.getConfigBean();
            try {
                this.connection = DriverManager.getConnection(configBean.getUrl(), configBean.getUsername(), configBean.getPassword());
                this.connection.setAutoCommit(false);
            } catch (SQLException e) {
                throw new YunQueException("数据库连接失败", e);
            }
        }

        @Override
        public DbTaskResult doProcess() {
            logger.debug("消息投递开始");
            return defaultStartRead();
        }

        @Override
        public DbTaskResult postProcess(DbTaskResult processResult) {
            try {
                this.connection.commit();
                this.connection.close();
            } catch (SQLException e) {
                throw new YunQueException("数据库关闭失败", e);
            }
            countDownLatch.countDown();
            logger.debug("消息投递完成");
            return processResult;
        }

        /**
         * 默认的线程读写处理逻辑，数据库可选择覆写逻辑，实现自己的读写
         */
        public DbTaskResult defaultStartRead() {
            long startTime = System.currentTimeMillis();
            int count = 0;
            DbTaskResult dbTaskResult = new DbTaskResult();
            ResultSet resultSet_ = null;
            try (PreparedStatement preparedStatement = connection.prepareStatement(this.splitSql);
                ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet_ = resultSet;
                while (resultSet.next()) {
                    DefaultRecord defaultRecord = new DefaultRecord();
                    ResultSetMetaData metaData = resultSet.getMetaData();
                    for (int i = 1; i <= metaData.getColumnCount(); i++) {
                        Column column = Transformer.buildColumn(resultSet, metaData, i);
                        defaultRecord.addColumn(column);
                    }
                    queue.add(defaultRecord);
                    count++;
                }
                dbTaskResult.ok();
            } catch (Exception e) {
                try {
                    assert resultSet_ != null;
                    dbTaskResult.bad(resultSet_.getRow() - count);
                } catch (SQLException ex) {
                    dbTaskResult.bad(0);
                    ex.printStackTrace();
                }
                e.printStackTrace();
            }
            dbTaskResult.setSuccessRowNum(count);
            dbTaskResult.setDuration(System.currentTimeMillis() - startTime);
            return dbTaskResult;
        }
    }

    public static long getCountDownCount() {
        return countDownLatch.getCount();
    }
}
