package www.larkmidtable.com;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import www.larkmidtable.com.reader.AbstractDBReader;

/**
 * @Description: [dorisreader]
 * @Author: tony 、fei
 */
public class DorisReader extends AbstractDBReader {
	private static final Logger logger = LoggerFactory.getLogger(DorisReader.class);

	@Override
	protected AbstractDbReadTask newDbReadTaskInstance() {
		return new DorisReadTask();
	}

	public class DorisReadTask extends AbstractDbReadTask {
		// 使用默认实现
	}

}
