package www.larkmidtable.com;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.larkmidtable.yunque.config.ConfigConstant;
import com.larkmidtable.yunque.system.MachineInfo;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import www.larkmidtable.com.bean.ConfigBean;
import www.larkmidtable.com.channel.ChannelV2;
import www.larkmidtable.com.channel.DefaultChannelV2;
import www.larkmidtable.com.exception.YunQueException;
import www.larkmidtable.com.log.LogRecord;
import www.larkmidtable.com.reader.Reader;
import www.larkmidtable.com.transformer.TransformerExecution;
import www.larkmidtable.com.transformer.TransformerInfo;
import www.larkmidtable.com.util.DBUtil;
import www.larkmidtable.com.util.ExitCode;
import www.larkmidtable.com.util.TransformerUtil;
import www.larkmidtable.com.writer.Writer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @Date: 2022/11/10 14:28
 * @Description: 云雀启动类
 **/
public class YunQueEngine {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws ParseException {

	}
}
